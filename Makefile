###############################################################################
###          University of Hawaii, College of Engineering
### @brief   Lab 02d - mouseNcat - EE 205 - Spr 2022
###
### @file    Makefile
### @version 1.0 - Initial version
###
###
### @author Ryan Edquiba 
### @date   1/26/22 
###
### @see     https://www.gnu.org/software/make/manual/make.html
###############################################################################

CC     = gcc
CFLAGS = -g -Wall

TARGET = mouseNcat

all: $(TARGET)

catNmouse: mouseNcat.cpp
	$(CC) $(CFLAGS) -o $(TARGET) mouseNcat.cpp

clean:
	rm -f $(TARGET) *.o


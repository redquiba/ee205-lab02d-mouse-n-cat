///////////////////////////////////////////////////////////////////////////////
//           University of Hawaii, College of Engineering
/// @brief   Lab02d - Mouse `n Cat - EE 205 - Spr 2022
///
/// This C++ program will either:
///   1.  Print the command line arguments in reverse order
///   2.  or, if there are no command line arguments, print all of the
///       environment variables passed into the program
///
/// @file    mouseNcat.cpp
/// @version 1.0 - Initial version
///
/// Compile: $ g++ -o mouseNcat mouseNcat.cpp
///
/// Usage:  mouseNcat [param1] [param2] ...
///
/// Example:
///   With a command line parameter:
///   $ ./mouseNcat I am Sam
///   Sam
///   am
///   I
///   $
///
///   Without a command line parameter:
///   $ ./mouseNcat
///   SHELL=/bin/bash
///   ...
///   SSH_TTY=/dev/pts/0
///
/// @author  Ryan Edquiba <redquiba@hawaii.edu>
/// @date    1/27/22
///////////////////////////////////////////////////////////////////////////////


#include <iostream>
#include <cstdlib>

// Note the new main() parameter:  envp
int main( int argc, char* argv[], char* envp[] ) {
  // std::cout << argc << std::endl;
   if(argc == 1)
   {
      for (int i = 0; envp[i] != NULL;  ++i) 
      std::cout << envp[i] << std::endl;
   }
   else
   {
      for (int j = argc; j > 0 ; --j)
      {
         std::cout << argv[j-1] << std::endl;
      }
   }
   std::exit( EXIT_SUCCESS );
}
